#################################
#             Shadows           #
#################################

# Enable client-side shadows on windows
shadow = true;

# Set shadow properties
shadow-radius = 7;
shadow-offset-x = -7;
shadow-offset-y = -7;
shadow-opacity = 0.75;

# Specify windows that should not have shadows
shadow-exclude = [
  "name = 'Notification'",
  "class_g = 'Conky'",
  "class_g ?= 'Notify-osd'",
  "class_g = 'Cairo-clock'"
];

#################################
#           Fading              #
#################################

# Enable fading
fading = true;

# Set fading properties
fade-in-step = 0.1;
fade-out-step = 0.1;
fade-delta = 8;

#################################
#   Transparency / Opacity      #
#################################

# Set opacity of inactive windows
inactive-opacity = 0.85;

# Set opacity of window titlebars and borders
frame-opacity = 0.9;

# Ensure inactive opacity does not override _NET_WM_WINDOW_OPACITY
inactive-opacity-override = false;

# Set opacity rules
opacity-rule = [
    "85:class_g = 'Alacritty'",
    "85:class_g = 'URxvt'",
    "85:class_g = 'XTerm'",
    "85:class_g = 'XFCE4-terminal'"
];

#################################
#           Corners             #
#################################

# Set the radius of rounded window corners
corner-radius = 5;

rounded-corners-exclude = [
  "window_type = 'dock'",
  "window_type = 'desktop'"
];

#################################
#     Background-Blurring       #
#################################

blur-kern = "3x3box";

blur-background-exclude = [
  "window_type = 'dock'",
  "window_type = 'desktop'"
];

#################################
#       General Settings        #
#################################

# Use Xrender backend
backend = "xrender";

# Enable vsync
vsync = true;

# Detect and mark focused windows
mark-wmwin-focused = true;
mark-ovredir-focused = true;
detect-rounded-corners = true;
detect-client-opacity = true;
detect-transient = true;

# Enable use-damage to improve performance
use-damage = true;

# Set log level
log-level = "warn";

#################################
#           Colors              #
#################################

# Set the active window color to deep dark purple (similar to Ubuntu Terminal)
# Note: This feature depends on your setup. You can set colors for shadows and borders.

shadow-color = "#4B0082" # Indigo color, close to dark purple

#################################
#       Window Types            #
#################################

wintypes:
{
  tooltip = { fade = true; shadow = true; opacity = 0.75; focus = true; full-shadow = false; };
  dock = { shadow = false; clip-shadow-above = true; }
  dnd = { shadow = false; }
  popup_menu = { opacity = 1; }
  dropdown_menu = { opacity = 1; }
};
