## Directories Needed
```
~/.icewm/
├── keys        # Keybindings configuration
├── menu        # Menu items
├── preferences # Main IceWM settings
├── startup     # Programs to start on IceWM launch
├── themes/     # Custom themes directory
├── toolbar     # Toolbar icons and settings
└── winoptions  # Window-specific options
```
## Requirements

`sudo apt update && sudo apt install conky-all fonts-dejavu lxterminal rox-filer rofi picom flameshot firefox-esr feh network-manager-gnome volumeicon-alsa cbatticon pasystray conky-all xfonts-terminus alsa-utils lm-sensors`
