# NOTE!!
This was made for **Debian 12**, on a T460 Thinkpad _laptop_, any other setups may require cherry picking or entirely reconfiguring chunks of my content. Big thank you to my i3 friends giving me ideas, and the fluxbox/openbox/busybox community and their documentation efforts because without them, I wouldn't had gotten this to work just the way I wanted it to.

# Example how to add desktop shortcuts
```
mkdir -p ~/Desktop
cd ~/Desktop
```


`nano firefox.desktop`

```
[Desktop Entry]
Version=1.0
Name=Firefox
Comment=Web Browser
Exec=firefox
Icon=firefox
Terminal=false
Type=Application
Categories=Network;WebBrowser;
```
do not forget to add permissions:
`chmod +x firefox.desktop`

# Packages requires or suggested
`apt update && apt install fluxbox mousepad slim conky-all procps rofi nitrogen picom i3lock redshift unclutter lxappearance pcmanfm firefox-esr notepadqq cherrytree btop fbrun alsa-utils `

## Music
`apt install audacious qmmp vlc`

These I do not remember well if it is in the default debian repo:
`apt install fbautostart slim lm-sensors pulseaudio`

`apt install procps `OR `apt install procps-ng` it tends to go by these two names

### !! This is setup for xorg only!!
`apt install xorg x11-xserver-utils xset xinput xbacklight xss-lock x11-utils xkill`

### Terminals
`apt install xterm tilix kitty xfce4-terminal alacritty`

### Fonts
`apt install xfonts-terminus`

### Filesystem Monitoring
The `internal/fs` module monitors filesystem usage. This functionality is typically built into the Linux system and doesn't require additional packages.

### Network Monitoring
For the internal/network, wlan, and eth modules, ensure you have network management tools installed _(net-tools, wireless-tools, iw, or NetworkManager depending on your setup)._
`apt install nm-applet `OR` apt install network-manager-gnome`

### Date/Time Module
The internal/date module just requires standard system date/time utilities, which are part of the core Linux utilities.

## Laptop specific
`apt install acpi` is required if using laptop
Backlight Control: The internal/backlight module needs access to /sys/class/backlight/. To control brightness, make sure you have appropriate permissions, and potentially the `xbacklight` package.

## Regarding the .ini file
The `session.styleFile` and `session.styleOverlay` settings reference specific style files (green_tea in my case).
Ensure that the fluxbox themes and styles are properly installed. This might be part of the fluxbox package or might require additional theme packages:

    Common path: /usr/share/fluxbox/styles/

If you need extra themes: `fluxbox-themes`

## Crontab
Scheduled Tasks:

    Log Rotation:
        Schedule: Every Sunday at midnight (0 0 * * 7)
        Command: /usr/sbin/logrotate /etc/logrotate.conf
        Description: Runs logrotate to rotate and compress log files as per the configuration in /etc/logrotate.conf.

    Cache Cleaning:
        Schedule: Every 14 days at midnight (0 0 */14 * *)
        Command: /usr/local/bin/clean_cache.sh
        Description: Executes a script to clean cache every two weeks.

    Empty Trash:
        Schedule: On the 1st day of every month at midnight (0 0 1 * *)
        Command: /usr/local/bin/empty_trash.sh
        Description: Runs a script to empty the trash folder monthly.

    Weekly System Update:
        Schedule: Every Sunday at midnight (0 0 * * 7)
        Command: /usr/local/bin/weekly_update.sh
        Description: Runs a script to update the system weekly.

Required Packages:

    Cron:
        The main service required to execute the scheduled tasks.
        Package: cron
        Ensure the cron service is running:

        

```
    sudo systemctl enable cron
    sudo systemctl start cron
```


Logrotate:

    Specifically needed for the log rotation task.
    Package: logrotate

**Custom Scripts (clean_cache.sh, empty_trash.sh, weekly_update.sh):**

    Ensure that these scripts are executable and located in /usr/local/bin/.
    Make the scripts executable:

    

```
        sudo chmod +x /usr/local/bin/clean_cache.sh
        sudo chmod +x /usr/local/bin/empty_trash.sh
        sudo chmod +x /usr/local/bin/weekly_update.sh
```


Summary of Required Packages:

    `cron`: To handle the scheduled tasks.
    `logrotate`: For managing log files.
    Custom Scripts: Ensure the scripts (clean_cache.sh, empty_trash.sh, weekly_update.sh) are executable and correctly placed OR remove the mention of them from the config file itself.

## Bash Scripts
These files are optional but the conky config relies on it somewhat plus some random cosmetics like sound effects or visuals. But some future bashfiles may be necessary to have this config setup work. Idk and I rarely keep these readme.md's up to date.