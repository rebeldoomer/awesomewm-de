#!/bin/bash

last_event_time=0
debounce_delay=2 # 2 seconds delay

# Monitor GNOME ScreenSaver signals
dbus-monitor --session "type='signal',interface='org.gnome.ScreenSaver'" | \
while read -r x; do
    current_time=$(date +%s)
    if [ $((current_time - last_event_time)) -lt $debounce_delay ]; then
        continue
    fi
    case "$x" in 
        *"boolean true"*) 
            paplay /home/reb/Music/Sound FX/windowsxpstartup_201910/chimes.wav
            last_event_time=$(date +%s)
            ;;
        *"boolean false"*) 
            paplay /home/reb/Music/Sound FX/windowsxpstartup_201910/notify.wav
            last_event_time=$(date +%s)
            ;;
    esac
done

