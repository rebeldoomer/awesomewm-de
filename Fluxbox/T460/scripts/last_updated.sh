#!/bin/bash

LOG_FILE="$HOME/.last_update_times"

# Log the last update time
echo "Last update: $(date '+%Y-%m-%d %H:%M:%S')" > "$LOG_FILE"

# Uncomment the actual update/upgrade commands you use:
sudo nala update
# sudo nala upgrade
