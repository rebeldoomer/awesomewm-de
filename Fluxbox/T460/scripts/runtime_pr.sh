#!/bin/bash

LOG_FILE="$HOME/.uptime_highscore"
CURRENT_UPTIME=$(awk '{print $1}' /proc/uptime | cut -d'.' -f1)
HIGH_SCORE=$(cat "$LOG_FILE" 2>/dev/null || echo 0)

if [ "$CURRENT_UPTIME" -gt "$HIGH_SCORE" ]; then
    echo "$CURRENT_UPTIME" > "$LOG_FILE"
fi
